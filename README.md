Adding and rotating keys
------------------------

Preferablly load archive signer keys into an ephemeral GnuPG homdir,
strip off everything but the signing keys, and then export thusly:
```
for user in user1 user2
do
  gpg --armor --export-options export-minimal --export "$user" > $user".asc
done
```

Worked example
------------------------

- get a copy of the key to be replaced into a file to import and clean
- make an ephemeral directory to work in:
    - ```$ mkdir $(pwd)/gpg-temphome```
- import key file into temporary GnuPG homedir
    - ```$ gpg --homedir $(pwd)/gpg-temphome --import ~/erik.signed.gpg```
    - ```
      gpg: keybox 'gpg-temphome/pubring.kbx' created
      gpg: key 13B2D6893DAF61BE: 1 signature not checked due to a missing key
      gpg: gpg-temphome/trustdb.gpg: trustdb created
      gpg: key 13B2D6893DAF61BE: public key "Erik Robert Graham von Reis <evonreis@caltech.edu>" imported
      gpg: Total number processed: 1
      gpg:               imported: 1
      gpg: no ultimately trusted keys found
      ```
- find its ID and confirm that it matches your expectation
    - ```$ gpg --homedir $(pwd)/gpg-temphome --list-keys --keyid-format 0xlong```
    - ```
      gpg-temphome/pubring.kbx
      ------------------------------------------------------------
      pub   rsa2048/0x13B2D6893DAF61BE 2020-09-14 [SC] [expires: 2022-09-17]
            E92063DCBBACDF97E7150C0E13B2D6893DAF61BE
            uid                   [ unknown] Erik Robert Graham von Reis <evonreis@caltech.edu>
            sub   rsa2048/0x710E2DB4C62EE349 2020-09-14 [E] [expires: 2022-09-17]
      ```
- export it into an ASCII-armored file for the build to use
    - ```
      $ gpg --homedir $(pwd)/gpg-temphome \
            --armor --export-options export-minimal \
            --export E92063DCBBACDF97E7150C0E13B2D6893DAF61BE \
        > evonreis.asc
      ```
- add new log entry to debian/changelog to bump the version number of the package (date format is ```date -R``` [for RFC5322 format])
